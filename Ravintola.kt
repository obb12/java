import factorymethod.Opettaja
import factorymethod.Opiskelija
import factorymethod.Opo

internal object Ravintola {
    @JvmStatic
    fun main(args: Array<String>) {
        val opettaja = Opettaja()
        opettaja.aterioi()
        val opiskelija = Opiskelija()
        opiskelija.aterioi()
        val opo = Opo()
        opo.aterioi()
    }
}